function scrollToElement(idclick, idpoint){

    if(typeof idclick === 'string' && typeof idpoint === 'string'){
        let clickbuttom = document.querySelector(idclick);
        let point = document.querySelector(idpoint)

        if(clickbuttom && point ){
            clickbuttom.addEventListener('click', function(){
                point.scrollIntoView({
                    behavior: "smooth", 
                    block: "start", 
                    inline: "nearest",
                })
            })
        }
    }
}

scrollToElement('#ch','bt');